const dataword = [
    {
      words:[],
      result: '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : เป็นคำถามที่ไม่มีคำตอบในระบบ กรุณารอแอดมินมาตอบสักครู่นะครับ',
      IDquestion:00
    },
    {
      words: [
        "วิธี",
        "ค้นหา",
        "หนังสือ",
        "web",
        "Web",
        "opac",
        "OPAC",
        "Opac",
        "จอง",
        "ทำอย่างไร",
      ],
      result:
        "http://dbsrv.lib.buu.ac.th/phpmyfaq/index.php?action=artikel&cat=1&id=54&artlang=th",
      IDquestion:01
    },
    {
      words: ["จอง", "ห้อง", "ศึกษา", "กลุ่ม", "ทำอย่างไร", "ได้หรือไม่", "ได้", "ไหม"],
      result:
      '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : \n1. ทำการจองผ่านระบบออนไลน์ โดยเข้าไปที่เว็บไซต์สำนักหอสมุด www.lib.buu.ac.th' +         
      "\n2. เลือกในส่วน Online Service คลิกที่ บริการจองห้องค้นคว้ากลุ่ม" +         
      "\n3. Log in เข้าสู่ระบบ โดยใช้รหัสและรหัสผ่านที่ได้จากสำนักคอมพิวเตอร์" +         
      "\n4. เลือก วันที่ที่ต้องการทำการจอง" +         
      "\n5. เลือก พื้นที่" +         
      "\n6. คลิกเลือก ช่วงเวลา (ผู้ใช้บริการต้องเข้าใช้ตั้งแต่ 3 คนขึ้นไป และใช้ได้ครั้งละ 2 ชั่วโมง)" +         
      "\n7. กรอกรายละเอียดการจองให้ครบถ้วน และคลิกที่ บันทึก" +         
      "\n8. ติดต่อเจ้าหน้าที่เพื่อขอรับกุญแจห้อง ณ ชั้นที่จอง และตามเวลาที่จอง",
        IDquestion:02
    },
    {
      words: ["สวัสดี", "Hello", "hello"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : สวัสดีครับนี้เป็นบอทสำหรับตอบคำถามเกี่ยวกับข้อมูลของหอสมุด',
        IDquestion:03
    },
    {
      words: ["เวลา", "เปิด", "ปิด","หอสมุด","กี่โมง","ตอนไหน","เมื่อไหร่"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : เวลาเปิดหอสมุด 8.00 น. เวลาปิดหอสมุด 20.00 น. และชั้น 1 ปิด 22.00 น.',
        IDquestion:04
    },
    {
      words: [
        "ต้องการ",
        "จอง",
        "ห้อง",
        "ประชุม",
        "ว่าง",
        "หรือไม่",
        "ค้นคว้า",
        "กลุ่ม",
        "ชั้นไหน",
      ],
      result: "http://www.lib.buu.ac.th/roombooking",
      IDquestion:05
    },
    {
      words: ["ลืม", "บัตร", "นิสิต", "สามารถ", "ยืม", "หนังสือ","ได้","ไหม","ได้หรือไม่"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : โดยปกติจะไม่ได้ครับ ยกเว้นสามารถยืนยันตัวตนเช่น ใช้บัตรประชาชน',
        IDquestion:06
    },
    {
      words: ["สามารถ", "ยืม", "วารสาร", "ได้", "ไหม", "ได้หรือไม่"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : ไม่สามารถยืมได้ แต่สามารถนำมาถ่ายเอกสารที่ร้านถ่ายเอกสาร ชั้น 3 ได้ครับ',
        IDquestion:07
    },
    {
      words: ["สามารถ", "ยืม", "ปลั๊กไฟ", "ได้หรือไม่", "ได้", "ไหม"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : ' +
        "\n1. ผู้ ใช้บริการไม่สามารถนำปลั๊กไฟที่มีให้บริการ ลงไปใช้ที่ห้องโถงชั้น 1 ได้ สามารถใช้ได้ภายในห้องสมุดบริเวณ ชั้น 2-7 เท่านั้น ซึ่งสามารถยืมปลั๊กไฟได้ที่เคาน์เตอร์บริการ ชั้น 2-6" +
        "\n2. ผู้ใช้บริการต้องคืนปลั๊กไฟที่เคาน์เตอร์ ชั้น 2-6 ชั้นใดก็ได้ เมื่อใช้เสร็จแล้ว ไม่สามารถนำปลั๊กไฟออกนอกห้องสมุดได้",
        IDquestion:08
    },
    {
      words: ["บุคคล", "ภายนอก", "ยืม", "หนังสือ", "ได้หรือไม่", "ได้", "ไหม"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" : ได้ครับ แต่ต้องสมัครเป็นสมาชิกก่อน สามารถติดต่อขอสมัครได้ที่ชั้น 2 ครับ',
        IDquestion:09
    },
    {
      words: ["ยืม", "หนังสือ", "ต้องทำ", "ยังไง", "อย่างไร","วิธี"],
      result:
        '"นี้เป็นข้อความตอบกลับอัตโนมัติ" :' +
        "\n1. เข้าสู่เว็บไซต์ สำนัก http://www.lib.buu.ac.th/webnew2/ คลิกที่ Web OPAC" +
        "\n 2. คลิกที่ สมาชิก ใส่รหัส นิสิต รหัสผ่าน ใส่รหัสนิสิต ." +
        "\n 3. คลิกเข้าสู่ระบบ คลิกที่ การยืมคืน" +
        "\n 4. คลิกที่ ยืมต่อ" +
        "\n 5. ใส่เครื่องหมาย / ที่ช่อง หน้าเลขบาร์โค้ดของหนังสือแต่ละเล่ม คลิกยืมต่อ" +
        "\n6. หากต้องการยืมต่อทุกรายการ ใส่เครื่องหมาย / ที่ช่องด้านบน" +
        "\n7. รายการที่ยืมต่อ วันที่จะเปลี่ยนโดยอัตโนมัติ" +
        "\n8. คลิกออกจากระบบ" +
        "\n**การยืมต่อสามารถยืมต่อได้ 1 ครั้ง",
        IDquestion:10
    },
  ];
  const word = "ปลั๊กไฟ";
  var countnumber = 0;
  var newcountnumber = -1;
  var textforsend;
  var IDquestion;
  for (var i = 0; i < dataword.length; i++) {
    for (const element of dataword[i].words) {
      var numberoffound = word.search(element);
      if (numberoffound >= 0) {
        countnumber++;
      }
    }
    if (countnumber > newcountnumber) {textforsend = dataword[i].result,IDquestion = dataword[i].IDquestion};
    newcountnumber = countnumber;
    countnumber = 0;
  }
  console.log(textforsend)



  function ai() {
    // for (var k = 1; k < 11; k++) {
        var countnumber = 0;
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("questions").findOne(question, function (err, result) {
                if (err) throw err;
                words = result.words.split(",")
                console.log(result.words)
                for (var i = 0; i < words.length; i++) {
                    setTimeout(() => {}, 500000)
                    var numberoffound = word.search(words[i])
                    if (numberoffound >= 0) {
                        countnumber++;
                    }
                }
                if (countnumber > 0) {
                    textforsend = result.result
                } else {
                    textforsend = noanswer
                }
                console.log(textforsend)
                num = parseInt(question.idquestion)
                console.log(question)
                num++
                question.idquestion = num.toString()
                db.close()
            })
        });
    }
// }